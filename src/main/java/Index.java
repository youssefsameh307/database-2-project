import java.util.ArrayList;

public abstract class Index {
	Index(ArrayList columns_index){
		// these are the indecies that the Index will work on 
		/*
		 	example : 
		 	Table : || Name | Age | GPA
		 			|| Ahmed| 18  | A
		 			
		 	columns_index = [0, 2] 
		 	so we will index based on columns Name and GPA together 
		 */
	}
	
	abstract void addPage(Page p); // add this page to index
	abstract void removePage(Page p); // update index with this page removed
	abstract void updatePage(Page p); // update index when this page is updated
	
}
