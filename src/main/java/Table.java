import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;



public class Table implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -2628645096996933045L;
	String tableName;
    String clusteringKey;
    Hashtable<String, String> colNameType;
    Hashtable<String, String> colNameMin;
    Hashtable<String, String> colNameMax;
    
    
    Vector<Vector<Hashtable<String, Object>>> pages = new Vector<Vector<Hashtable<String, Object>>>(0,1);
    
    
	Hashtable<String, Object> col_name_min_value;
	Hashtable<String, Object> col_name_max_value;
	
	Hashtable<String, String> keys = new Hashtable<String, String>();

//	final Hashtable<String, Object> defaultValues;
	
	int IdGenerator = 0;

    public Table(String tableName, String clusteringKey, Hashtable<String, String> colNameType, Hashtable<String, String> colNameMin, Hashtable<String, String> colNameMax, boolean writable) {
        this.tableName = tableName;
        this.clusteringKey = clusteringKey;
        this.colNameType = colNameType;
        this.colNameMin = colNameMin;
        this.colNameMax = colNameMax;
        
//        this.defaultValues = new Hashtable<String, Object>();
        
      if(writable)
        this.writeToMeta(true);
        
        try {
            this.set_table_properties_from_metadata_file();
//            for(String x: this.col_name_min_value.keySet()) {
//            	String type = getTypeCast(tableName, x);
//            	switch(type.toLowerCase()) {
//            	case "java.lang.double": this.defaultValues.put(x, 0.0);break;
//            	case "java.lang.string" : this.defaultValues.put(x, "null");break;
//                case "java.util.date": this.defaultValues.put(x, new Date(0-1900,1-1,1));break;
//                case "java.lang.integer" : this.defaultValues.put(x, 0);break;
//                default: throw new DBAppException("error in creating default values");
//            	}
//            }
        }catch(Exception e) {
        	if(e.getMessage() != null)
        		System.err.println(e.getMessage());
            //e.printStackTrace();
        }
        finally {
            if(writable)
                 this.SerializeTable();
		}
    }

    
    public  int assertRangeC(Hashtable<String,Object> record) throws IOException, DBAppException{
        for (String key:record.keySet()){
        	if( !((record.get(key)).getClass().getCanonicalName()).equalsIgnoreCase(getTypeCast(this.tableName, key)) )
            	return -1;
        	Comparable keyc = (record.get(key) instanceof String)? (Comparable) record.get(key).toString().toLowerCase() : (Comparable) record.get(key);
        	Comparable maxc = (record.get(key) instanceof String)? (Comparable) col_name_max_value.get(key).toString().toLowerCase() : (Comparable) col_name_max_value.get(key);
        	Comparable minc = (record.get(key) instanceof String)? (Comparable) col_name_min_value.get(key).toString().toLowerCase() : (Comparable) col_name_min_value.get(key);
        	if(keyc.compareTo(minc)<0)
                return 0;
            if(keyc.compareTo(maxc)>0)
                return 0;
            
            	
        }
        return 1;
    }
    
//    private ArrayList<Hashtable<String, Object>> LinearSearch(Hashtable<String, Object> search_criteria) {
//  		ArrayList<Hashtable<String, Object>> result_set = new ArrayList<Hashtable<String,Object>>();
//  		ArrayList<String> criteria = new ArrayList<String>();
//  		Enumeration enu = search_criteria.keys();
//  		while (enu.hasMoreElements()) {criteria.add((String) enu.nextElement());}
//  		for(Hashtable<String, Object> element : this.pages){
//  			String path_to_page = (String) element.get("path");
//  			Page p = this.loadPage(path_to_page);
//  			
//  			for( Hashtable<String, Object> row :p.content){
//  				boolean matches_search_criteria = true;
//  				for (String key : criteria) {
//  					boolean accepted = row.get(key).equals(search_criteria.get(key));
//  					if(!accepted){
//  						matches_search_criteria = false;
//  					}
//  				}
//  				
//  				if(matches_search_criteria){
//  					result_set.add(row);
//  				}
//  			}
//  		}
//  		return result_set;
//  	}
//
//    private ArrayList<Page> LinearSearchPages(Hashtable<String, Object> search_criteria) {
//    	ArrayList<Page> result_set = new ArrayList<Page>();
//  		ArrayList<String> criteria = new ArrayList<String>();
//  		Enumeration enu = search_criteria.keys();
//  		while (enu.hasMoreElements()) {criteria.add((String) enu.nextElement());}
//  		for(Hashtable<String, Object> element : this.pages){
//  			String path_to_page = (String) element.get("path");
//  			Page p = this.loadPage(path_to_page);
//  			
//  			for( Hashtable<String, Object> row :p.content){
//  				boolean matches_search_criteria = true;
//  				for (String key : criteria) {
//  					boolean accepted = row.get(key).equals(search_criteria.get(key));
//  					if(!accepted){
//  						matches_search_criteria = false;
//  					}
//  				}
//  				
//  				if(matches_search_criteria){
//  					result_set.add(p);
//  					break;
//  				}
//  			}
//  		}
//  		return result_set;
//  	}
 
    public int search(Comparable id) throws IOException {
        int size = pages.size();
        int lowIndex = 0;
        int highIndex=size-1;
        int elementpos=0;

        while (lowIndex <= highIndex) {

            int midIndex = (lowIndex + highIndex) / 2;
            Hashtable<String, Object> current = pages.get(midIndex).firstElement();
            Comparable currentmin = (Comparable) current.get("min");
            Comparable currentmax = (Comparable) current.get("max");
            Comparable nextpagemin=0;
            Comparable prevpagemax=0;
            if(midIndex!=size-1)
                nextpagemin =(Comparable) pages.get(midIndex+1).firstElement().get("min");
            if(midIndex!=0)
                prevpagemax = (Comparable) pages.get(midIndex-1).firstElement().get("max");


            if (currentmin.compareTo(id)<=0 && currentmax.compareTo(id)>=0) {
                elementpos = midIndex;
                return elementpos;
            }
            if(midIndex!=0&&currentmin.compareTo(id)>=0&&prevpagemax.compareTo(id)<=0){
            	
            	if(!Page.DeserializePage(pages.get(midIndex-1).firstElement().get("path").toString(), this).isFull())
                    elementpos=midIndex-1;
                else
                    elementpos = midIndex;
                return elementpos;
            }
            if(midIndex!=size-1&&currentmax.compareTo(id)<=0&&nextpagemin.compareTo(id)>=0){
            	if(!Page.DeserializePage(pages.get(midIndex).firstElement().get("path").toString(), this).isFull())
                    elementpos = midIndex;
                else
                    elementpos = midIndex+1;
                return elementpos;
            }
            else if (currentmin.compareTo(id)>0) { 
                highIndex = midIndex-1;
            } else if (currentmax.compareTo(id)<0) { 
                lowIndex = midIndex+1;
            }
        }
        if(highIndex<0)
            return -1;
        else
            return -2;
    }

    public boolean insert(Hashtable<String,Object> record) throws IOException, DBAppException {
    	if(assertRangeC(record) == -1)
        	throw new DBAppException("incompatible data type");
    	if(assertRangeC(record) == 0)
        	throw new DBAppException("data out of range");
//        Hashtable<String,Object> merged = Page.merge(this.defaultValues, record);
    	try {
            Comparable key = (Comparable) (record.get(this.clusteringKey));
            if(pages.isEmpty()){
                pages.add(new Vector<Hashtable<String, Object>>(0,1));
                Page newpage = new Page(record,this);
                SerializeTable();
                return true;
            }
            int pagenumber = search(key);
            if (pagenumber == -1) {
                Page.DeserializePage(pages.firstElement().firstElement().get("path").toString(), this).insert(record);
                SerializeTable();
                return true;
            }
            if (pagenumber == -2) {
                Page lastpage = Page.DeserializePage(pages.lastElement().firstElement().get("path").toString(), this);
                if (lastpage.isFull()) {
                    pages.add(new Vector<Hashtable<String, Object>>(0,1));
                    lastpage.nextpage = new Page(record,this);
                    SerializeTable();
                    return true;
                } else
                    lastpage.insert(record);
                SerializeTable();
                return true;
            }
            Page p = Page.DeserializePage(pages.get(pagenumber).firstElement().get("path").toString(), this);
            p.insert(record);
            try {
				pages.get(pagenumber).get(1);
				updateMinMax(p, pagenumber, null);
			} catch (ArrayIndexOutOfBoundsException e) {
				
			} finally {
				SerializeTable();
	            return true;
			}
            
        }
        catch (Exception e){
            System.err.println("error has occurred while inserting");
            return false;
        }
    }
    
//    public Page get_page(Hashtable<String,Object> record) throws DBAppException {
//    	    	
//    	Object search_key = record.get(this.clusteringKey);
//    	String path = binarySearch(this.pages, 0, this.pages.size(), search_key);
//    	if(path == null)
//    		throw new DBAppException("could not find a page to insert value in");
//    	
//    	return this.loadPage(path);
//    }
//    
//    public static String  binarySearch(Vector<Hashtable<String, Object>> pages2, int first, int last, Object search_key) {  
//    	
//    	
//        if (last>first){  
//            int mid = (int)(Math.round(first + (last - first)/2.0)) -1;
//            Comparable index1 = (Comparable) pages2.get(mid).get("min");
//            Comparable index2 = (Comparable) pages2.get(mid+1).get("min");
//            if (index1.compareTo(search_key) <= 0 && (index2.compareTo(search_key) >= 0)){  
//            	return (String) pages2.get(mid).get("path");
//            }  
//            if (index1.compareTo(search_key) > 0){  
//            return binarySearch(pages2, first, mid+1, search_key);
//            }else{  
//            return binarySearch(pages2, mid+1, last, search_key);
//            }  
//        }  
//        return (String) pages2.lastElement().get("path");
//        
//    }  
    
    public void show(Comparable key) throws IOException, DBAppException {
        int pagenum = search(key);
        if (pagenum < 0){
            System.err.println("not found");
        }
        else{
            Page.DeserializePage(pages.get(pagenum).firstElement().get("path").toString(), this).show(key);
        }
    }

    public boolean update(Comparable key, Hashtable<String,Object> record) throws IOException, DBAppException {
    	if(assertRangeC(record) == -1)
        	throw new DBAppException("incompatible data type");
    	if(assertRangeC(record) == 0)
        	throw new DBAppException("data out of range");
    	int pagenum = search(key);
        if (pagenum < 0){
            System.err.println("not found");
            return false;
        }
        else{
        	Page.DeserializePage(pages.get(pagenum).firstElement().get("path").toString(), this).update(key, record);
            SerializeTable();
            return true;
        }
    }
    
    public void updateMinMax(Page p, int position, Comparable deleted) {
    	Vector<Hashtable<String, Object>> x = this.pages.get(position);
    	Comparable temp = p.getmin();
    	p.setmin(p.getmax());
    	p.setmax(temp);
    	//p.setmax((Comparable) this.col_name_min_value.get(this.clusteringKey));
    	//p.setmin((Comparable) this.col_name_max_value.get(this.clusteringKey));
    	boolean skip = true;
    	for(Hashtable<String, Object> h: x) {
    		Comparable currentMin = (Comparable) h.get("min");
    		Comparable currentMax = (Comparable) h.get("max");
    		if(!currentMin.equals(deleted) && p.getmin().compareTo(currentMin)>0)
    			p.setmin(currentMin);
        	if(!currentMax.equals(deleted) && p.getmax().compareTo(currentMax)<0)
        		p.setmax(currentMax);
    	}
    	p.updatePage();
    }
    
    public void updateOverflow(Page original, int pagenum, int emptyP) throws IOException {
    	int lastposition = emptyP;
		try {
			while(true) {
				Page p = Page.DeserializePage(pages.get(pagenum).get(emptyP+1).get("path").toString(), this);
				lastposition = p.overflow--;
				p.updatePage();
				emptyP++;
			}
		} catch(ArrayIndexOutOfBoundsException e) {
        	this.pages.get(pagenum).remove(lastposition);
    		 
			File del2 = new File("./src/main/resources/data/"+this.tableName+"_"+original.ID+"_O"+lastposition+".ser");
        	Files.delete(del2.toPath());
		}
    }
    
    public void updateOriginal(int pagenum) throws IOException {
    	Vector<Page> temp = new Vector<Page>();
    	//Vector<File> del = new Vector<File>();
    	try {
			while(true) {
				Vector<Hashtable<String, Object>> v1 = this.pages.get(pagenum);
				Vector<Hashtable<String, Object>> v2 = this.pages.get(pagenum+1);
				Page p;
				for(Hashtable<String, Object> x: v2) {
					p = Page.DeserializePage(x.get("path").toString(), this);
					File del = new File(x.get("path").toString());
					Files.delete(del.toPath());
					p.ID--;
					temp.add(p);
				}
				this.pages.set(pagenum++, v2);
				for(Page x: temp) {
					x.updatePage();
				}
				temp.clear();
			}
    		
		} catch (ArrayIndexOutOfBoundsException e) {
			this.pages.remove(pagenum);
		}
    	
    	
    	
    	
    	
//    	Vector<File> temp = new Vector<File>();
//    	int i=0;
//    	for(Hashtable<String, Object> x: pages.get(pagenum+1)) {
//    		i++;
//    		Page p = Page.DeserializePage(x.get("path").toString(), this);
//    		//p.ID--;
//    		//temp.add(p);
//    		int of = (i==pages.get(pagenum+1).size())? p.overflow:p.overflow+1;
//    		File del;
//    		if(of == 0)
//    			del = new File("./src/main/resources/data/"+this.tableName+"_"+p.ID--+".ser");
//    		else
//    			del = new File("./src/main/resources/data/"+this.tableName+"_"+p.ID--+"_O"+of+".ser");
//    		if(!temp.contains(del))
//    			temp.add(del);
//    		//Files.delete(del.toPath());
//    		p.updatePage();
//    	}
//    	for(File p:temp) {
//    		Files.delete(p.toPath());
//    	}
    	
//        Page p = Page.DeserializePage(pages.get(pagenum+1).firstElement().get("path").toString(), this);
//    	try {
//    	do {
//		p.ID--;
//		p.updatePage();
//		pagenum++;
//		p = Page.DeserializePage(pages.get(pagenum).firstElement().get("path").toString(), this);
//		} while(p != null);
//    	}
//    	catch(ArrayIndexOutOfBoundsException e) {
//			pagenum--;
//    		this.pages.remove(pagenum);
//    		IdGenerator--;
//    		File del = new File("./src/main/resources/data/"+this.tableName+"_"+IdGenerator+".ser");
//    		Files.delete(del.toPath());
//    	}
//    	pagenum++;
//		this.pages.remove(pagenum);
//		IdGenerator--;
//		File del = new File("./src/main/resources/data/"+this.tableName+"_"+IdGenerator+".ser");
//		Files.delete(del.toPath());
    	
    	
    	
    	
    	
    	
    	
    	//Page p = Page.DeserializePage(pages.get(pagenum+1);
    	
    	
//    	try {
//    		do {
////    			boolean overflow = false;
////    			int lastO = p.overflow;
////    			while(p.overflow>0) {
////    				overflow = true;
////            		Page p2 = Page.DeserializePage(pages.get(pagenum).get(p.overflow).get("path").toString(), this);
////            		p2.overflow--;
////            		p2.updatePage();
////            		p2 = p2.nextpage;
////    			}
////    			if(overflow) {
////    				File del2 = new File("./src/main/resources/data/"+this.tableName+"_O"+lastO+".ser");
////            		Files.delete(del2.toPath());
////    			}
//    		
//    			p.ID--;
//    			p.updatePage();
//    			pagenum++;
//    			p = Page.DeserializePage(pages.get(pagenum).firstElement().get("path").toString(), this);
//    			} while(p != null);
//    	} catch(ArrayIndexOutOfBoundsException e) {
//			pagenum--;
//    		this.pages.remove(pagenum);
//    		IdGenerator--;
//    		File del = new File("./src/main/resources/data/"+this.tableName+"_"+IdGenerator+".ser");
//    		Files.delete(del.toPath());
//    	}
    }
    
    public boolean delete(Comparable key) throws IOException {
    	
        int pagenum = search(key);
        if (pagenum < 0){
            System.err.println("not found");
            return false;
        }
        else{
        	boolean updated = false;
        	boolean isoriginal = true;
        	boolean empty = false;
        	int emptyP = -1;
    		Page original = Page.DeserializePage(this.pages.get(pagenum).firstElement().get("path").toString(), this);
        	for(Hashtable<String, Object> x: this.pages.get(pagenum)) {
        		emptyP++;
        		Page p;
        		if(isoriginal) {
        			p = original;
        			isoriginal = false;
        		}
        		else {
        			p = Page.DeserializePage(x.get("path").toString(), this);
        		}
        		Boolean del = p.delete(key);
        		if(del && !original.isEmpty()) {
        			p.updatePage();
    				updateMinMax(original, pagenum, key);
        		}
        		if(del && original.isEmpty() && !updated) {
    				updateMinMax(p, pagenum, key);
    				updated = true;
        		}
        		if(del) {
        			if(p.isEmpty() && p != original)
        				empty = true;
        			break;
        		}
        			
        		//Page.DeserializePage(pages.get(pagenum).firstElement().get("path").toString(), this).delete(key);
        	}
        	if(empty) {
    			updateOverflow(original, pagenum, emptyP);
        	}
        	
        	
        	Page x = Page.DeserializePage(pages.get(pagenum).firstElement().get("path").toString(), this);
            if(pages.size()>0 && x.isEmpty()) {
            	try {
            		Page p = Page.DeserializePage(pages.get(pagenum).get(1).get("path").toString(), this);
            		updateMinMax(p, pagenum, key);
            		updateOverflow(x, pagenum, 0);
            	} catch(ArrayIndexOutOfBoundsException e) {
                	updateOriginal(pagenum);
            	}
            }
            SerializeTable();
            return true;
        }


    }
    
    public static ArrayList<String> readFromMeta() {
        try {
            File F = new File("./src/main/resources/metadata.csv");
            FileReader FR = new FileReader(F);
            BufferedReader BR = new BufferedReader(FR);

            ArrayList<String> previousDetails = new ArrayList<>();
            while(BR.ready())
                previousDetails.add(BR.readLine());
            BR.close();
            return previousDetails;
        } catch (Exception e) {
            System.err.println("Error in reading from metadata.csv");
            return null;
        }
    }

    public void writeToMeta(boolean keepOld) {
        ArrayList<String> oldData = readFromMeta();

        try {
            File F = new File("./src/main/resources/metadata.csv");
            FileWriter FW = new FileWriter(F);
            BufferedWriter BW = new BufferedWriter(FW);

            String[][] columnTypes = new String[this.colNameType.size()][2];
            String[][] columnMinimum = new String[this.colNameType.size()][2];
            String[][] columnMaximum = new String[this.colNameType.size()][2];

            int j=0;
            for(Entry<String, String> x: this.colNameType.entrySet()) {
                columnTypes[j][0] = x.toString().split("=")[0];
                columnTypes[j][1] = x.toString().split("=")[1];
                j++;
            }

            j=0;
            for(Entry<String, String> x: this.colNameMin.entrySet()) {
                columnMinimum[j][0] = x.toString().split("=")[0];
                columnMinimum[j][1] = x.toString().split("=")[1];
                j++;
            }

            j=0;
            for(Entry<String, String> x: this.colNameMax.entrySet()) {
                columnMaximum[j][0] = x.toString().split("=")[0];
                columnMaximum[j][1] = x.toString().split("=")[1];
                j++;
            }

            if(keepOld) {
                for(String x: oldData) {
                    BW.write(x);
                    BW.write("\r\n");
                }
            }

            for(int i=0; i<this.colNameType.size(); i++) {
                String tableDetails = this.tableName + ", " + columnTypes[i][0] + ", " +
                        columnTypes[i][1] + ", " + this.clusteringKey.equals(columnTypes[i][0]) + ", " +
                        "false, " + columnMinimum[i][1] + ", " + columnMaximum[i][1];
                BW.write(tableDetails);
                BW.write((i == this.colNameType.size() - 1)? "":"\r\n");
            }
            BW.write("\r\n");
            BW.close();
        } catch (Exception e) {
            System.err.println("Error in writing to metadata.csv");
        }
    }
    
    public static ArrayList<String> getTableNamesFromMeta(){
    	ArrayList<String> names = new ArrayList<String>();
    	ArrayList<Table> tables = loadTablesFromMeta();
    	for(Table t: tables)
    		names.add(t.tableName);
    	return names;
    }

    public static ArrayList<Table> loadTablesFromMeta() {
        ArrayList<Table> tables = new ArrayList<Table>();
        ArrayList<String[]> tablesAsString = new ArrayList<String[]>();

        for(String x:readFromMeta())
            tablesAsString.add(x.split(", "));

        Hashtable htblColNameType = new Hashtable ( );
        Hashtable htblColNameMin = new Hashtable ( );
        Hashtable htblColNameMax = new Hashtable ( );
        String clusterKey = "";

        for(int i=0; i<tablesAsString.size()-1; i++) {
            if(tablesAsString.get(i)[0].equals(tablesAsString.get(i+1)[0])) {
                htblColNameType.put(tablesAsString.get(i)[1], tablesAsString.get(i)[2]);
                htblColNameMin.put(tablesAsString.get(i)[1], tablesAsString.get(i)[4]);
                htblColNameMax.put(tablesAsString.get(i)[1], tablesAsString.get(i)[5]);

                if(Boolean.valueOf(tablesAsString.get(i)[3]))
                    clusterKey = tablesAsString.get(i)[1];
            }
            else {
                htblColNameType.put(tablesAsString.get(i)[1], tablesAsString.get(i)[2]);
                htblColNameMin.put(tablesAsString.get(i)[1], tablesAsString.get(i)[4]);
                htblColNameMax.put(tablesAsString.get(i)[1], tablesAsString.get(i)[5]);

                if(Boolean.valueOf(tablesAsString.get(i)[3]))
                    clusterKey = tablesAsString.get(i)[1];

                tables.add(new Table(tablesAsString.get(i)[0], clusterKey,
                        (Hashtable<String, String>) htblColNameType.clone(),
                        (Hashtable<String, String>) htblColNameMin.clone(),
                        (Hashtable<String, String>) htblColNameMax.clone(), false));

                htblColNameType.clear();
                htblColNameMin.clear();
                htblColNameMax.clear();
                clusterKey = "";
            }
        }

        htblColNameType.put(tablesAsString.get(tablesAsString.size()-1)[1], tablesAsString.get(tablesAsString.size()-1)[2]);
        htblColNameMin.put(tablesAsString.get(tablesAsString.size()-1)[1], tablesAsString.get(tablesAsString.size()-1)[4]);
        htblColNameMax.put(tablesAsString.get(tablesAsString.size()-1)[1], tablesAsString.get(tablesAsString.size()-1)[5]);

        if(Boolean.valueOf(tablesAsString.get(tablesAsString.size()-1)[3]))
            clusterKey = tablesAsString.get(tablesAsString.size()-1)[1];

        tables.add(new Table(tablesAsString.get(tablesAsString.size()-1)[0], clusterKey,
                (Hashtable<String, String>) htblColNameType.clone(),
                (Hashtable<String, String>) htblColNameMin.clone(),
                (Hashtable<String, String>) htblColNameMax.clone(), false));

        htblColNameType.clear();
        htblColNameMin.clear();
        htblColNameMax.clear();
        clusterKey = "";

        return tables;
    }


    public void validateMetaDataFile() throws MetaDataException, IOException {
        File F = new File("./src/main/resources/metadata.csv");
        FileReader FR = new FileReader(F);
        BufferedReader BR = new BufferedReader(FR);
        ArrayList<String> data = (ArrayList<String>) Files.readAllLines(Paths.get("./src/main/resources/metadata.csv"));
        BR.close();

        Hashtable<String, String> htbl_colname_type = new Hashtable<>();
        Hashtable<String, String> htbl_colname_min = new Hashtable<>();
        Hashtable<String, String> htbl_colname_max = new Hashtable<>();
        for (String x : data) {
            String[] properties = x.split(",");
            
            if (!properties[0].trim().equals(this.tableName))
                continue;
            String col_name = properties[1].trim();
            String col_type = properties[2].trim();
            String col_min = properties[5].trim();
            String col_max = properties[6].trim();
            htbl_colname_type.put(col_name, col_type);
            htbl_colname_min.put(col_name, col_min);
            htbl_colname_max.put(col_name, col_max);

            if (col_min.compareTo(col_max) > 0) {
                throw new MetaDataException("corrupt MetaData File : min is bigger than maximum");
            }
            try {
                Double d = new Double(18);
                Class t = d.getClass();
                System.out.println(t.getName());
                System.out.println(col_type);
                System.out.println(t.getName().equals(col_type));
                Class.forName(t.getName());
                Class.forName(col_type);

            } catch (ClassNotFoundException e) {
                //e.printStackTrace();
            	if(e.getMessage() != null)
            		System.err.println(e.getMessage());
                throw new MetaDataException("corrupt MetaData File : "+ col_name+ " " + col_type + " class is not found" );


            }

        }
    }

    public void set_table_properties_from_metadata_file() throws IOException, ClassNotFoundException, DBAppException, ParseException {
        File F = new File("./src/main/resources/metadata.csv");
        FileReader FR = new FileReader(F);
        BufferedReader BR = new BufferedReader(FR);
        ArrayList<String> data = (ArrayList<String>) Files.readAllLines(Paths.get("./src/main/resources/metadata.csv"));
        BR.close();

        Hashtable<String, String> htbl_colname_type = new Hashtable<>();
        Hashtable<String, Object> htbl_colname_min = new Hashtable<>();
        Hashtable<String, Object> htbl_colname_max = new Hashtable<>();
        for (String x : data) {
            String[] properties = x.split(",");
            
            if (!properties[0].equals(this.tableName)) {
                continue;
            }

            String col_name = properties[1].trim();
            String col_type = properties[2].trim();
            Object col_min = get_parsed_value(properties[5].trim(), col_type);
            Object col_max = get_parsed_value(properties[6].trim(), col_type);
            htbl_colname_type.put(col_name, col_type);
            htbl_colname_min.put(col_name, col_min);
            htbl_colname_max.put(col_name, col_max);
        }
        this.colNameType = htbl_colname_type;
        this.col_name_min_value = htbl_colname_min;
        this.col_name_max_value = htbl_colname_max;
        System.out.println(this.colNameType);

    }
    public static Object get_parsed_value(String string, String col_type) throws DBAppException, ParseException {
    	switch(col_type.trim().toLowerCase()) {
            case "java.lang.double": return Double.parseDouble(string);
            case "java.lang.string" : return string;
            case "java.util.date":return new SimpleDateFormat("yyyy-MM-dd").parse(string);  
            case "java.lang.integer" :return Integer.parseInt(string);
            default : throw new DBAppException("unrecognized class " + col_type);
        }

    }

    private boolean verify_class(Object value, String string) {
        System.out.println(string);
        System.out.println(value.getClass());
        Object temp ;
        switch(string.trim().toLowerCase()) {
            case "java.lang.double": temp = (double) value;break;
            case "java.lang.integer":temp= (int) value;break;
            case "java.lang.date": temp = (Date) value;break;
            case "java.lang.String": temp =  (String) value;break;
        }
        return true;
    }

    private void validate_input(Hashtable<String, Object> htblColNameValue) throws InvalidEntry {

        for (Entry<String, Object> colName_colValue : htblColNameValue.entrySet()) {
            String col_name = colName_colValue.getKey();
            Object value = colName_colValue.getValue();
            Class c;
            Class c2;
            try {
                c = Class.forName(colNameType.get(col_name));
                c2 = value.getClass();
                if (!verify_class(value, this.colNameType.get(col_name))) {
                    throw new InvalidEntry("the value of column " + col_name + " is not of type " + c.getSimpleName());

                }
            } catch (ClassNotFoundException | ClassCastException e ) {
                throw new InvalidEntry("the value of column " + col_name + " is not of the right type " + colNameType.get(col_name));
            }

            Comparable min = (Comparable) this.col_name_min_value.get(col_name);
            Comparable max = (Comparable) this.col_name_max_value.get(col_name);
            if (!(min.compareTo(value) <= 0 && max.compareTo(value) >= 0)) {
                throw new InvalidEntry(
                        "the value of column " + col_name + " does not fall between the values " + min + " and " + max);
            }

        }
    }
    
    public static String getTypeCast(String tableName, String Key) throws IOException, DBAppException  {
        File F = new File("./src/main/resources/metadata.csv");
        FileReader FR = new FileReader(F);
        BufferedReader BR = new BufferedReader(FR);
        ArrayList<String> data = (ArrayList<String>) Files.readAllLines(Paths.get("./src/main/resources/metadata.csv"));
        BR.close();
        
        
        for(String x: data) {
            if(x.split(",")[0].equals(tableName) && x.split(",")[1].substring(1, x.split(",")[1].length()).equals(Key))
                return x.split(",")[2].substring(1, x.split(",")[2].length());
        }
        throw new DBAppException("something wrong in the data type or column does not exist");
    }
    
    public void SerializeTable() {
        String  path = "./src/main/resources/data/"+tableName+".ser";
        try {
            FileOutputStream file = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(file);

            out.writeObject(this);
            out.close();
            file.close();

            System.out.println("Table serialized to path:" + path);
        }catch(IOException e){
        	if(e.getMessage() != null)
        		System.err.println(e.getMessage());
            //e.printStackTrace();
        }
    }
    
    public static Table DeserializeTable(String path) {

        Table table = null;

        try {

            FileInputStream file = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(file);

            table = (Table)in.readObject();

            in.close();
            file.close();

            System.out.println("Table deserialized!");

        }catch(IOException e){
        	if(e.getMessage() != null)
        		System.err.println(e.getMessage());
        	//e.printStackTrace();
        }catch(ClassNotFoundException e){
            System.err.println("Table not found!");
            //e.printStackTrace();
        }

        return table;
    }
    
    /*public static void main(String[] args) throws Exception {
    	
//    	test_search();
//    	System.out.println(getTypeCast("students", "gpa"));
    	
//        String strTableName = "Student";
//
//        Hashtable<String,String> htblColNameType = new Hashtable<String,String> ( );
//        htblColNameType.put("id", "java.lang.Integer");
//        htblColNameType.put("name", "java.lang.String");
//        htblColNameType.put("gpa", "java.lang.Double");
//
//        Hashtable<String,String> htblColNameMin = new Hashtable<String,String> ( );
//        htblColNameMin.put("id", "0");
//        htblColNameMin.put("name", "A");
//        htblColNameMin.put("gpa", "0.0");
//
//        Hashtable<String,String> htblColNameMax = new Hashtable<String,String> ( );
//        htblColNameMax.put("id", "1000000");
//        htblColNameMax.put("name", "ZZZZZZZZZZZ");
//        htblColNameMax.put("gpa", "5.0");
//
//        Table T = new Table( strTableName, "id", htblColNameType, htblColNameMin, htblColNameMax, false);
//
//        Hashtable<String,Object> h1 = new Hashtable<String,Object> ( );
//        h1.put("id", 2343432 );
//        h1.put("name", new String ("Ahmed Noor" ) );
//        h1.put("gpa", 0.95  );
//        T.insert(h1);
//
//        Hashtable<String,Object> h2 =new Hashtable<String,Object> ( );
//
//        h2.put("id", 453455 );
//        h2.put("name", new String ("Ahmed Noor" ) );
//        h2.put("gpa", 0.95 );
//        T.insert(h2);
//
//
//
//        Hashtable<String,Object> h3 = new Hashtable<String,Object> ( );
//
//        h3.put("id",5674567 );
//        h3.put("name", new String ("Dalia Noor" ) );
//        h3.put("gpa", 1.25 );
//        T.insert(h3);
//
//        Hashtable<String,Object> h4 = new Hashtable<String,Object> ( );
//
//        h4.put("id", 23498 );
//        h4.put("name", new String ("John Noor" ) );
//        h4.put("gpa", 1.5 );
//        T.insert(h4);
//
//        Hashtable<String,Object> h5 = new Hashtable<String,Object> ( );
//
//        h5.put("id", 78452 );
//        h5.put("name", new String ("Zaky Noor" ) );
//        h5.put("gpa", 0.88 );
//        T.insert(h5);
//
//
//        //System.out.println(T.pages.firstElement().getmin());
//       // System.out.println(T.pages.firstElement().getmax());
//
//        Hashtable<String,Object> h7 = new Hashtable<String,Object> ( );
//
//        h7.put("id", 234 );
//        h7.put("name", new String ("John Noor" ) );
//        h7.put("gpa", 1.5 );
//        T.insert(h7);
//
//
//        Hashtable<String,Object> h8 = new Hashtable<String,Object> ( );
//
//        h8.put("id", 123498 );
//        h8.put("name", new String ("John Noor" ) );
//        h8.put("gpa", 1.5 );
//        T.insert(h8);
//
//
//        Hashtable<String,Object> h6 = new Hashtable<String,Object> ( );
//        h6.put("name", new String ("test" ) );
//
//        T.update(23498,h6);
//        T.delete(78452);
//        System.out.println(1);
//        int[] records = {23498, 78452, 234, 123498, 5674567, 453455, 2343432, 11};
//
//        Hashtable<String,Object> h9 = new Hashtable<String,Object> ( );
//        h9.put("id", 30000 );
//        h9.put("name", new String ("s7s Noor" ) );
//        h9.put("gpa", 1.5 );
//        T.insert(h9);

        //Arrays.sort(records);
        //T.pages.firstElement().print();

        
////        System.out.println("karim tests"); ////////////////////////// KIKO TESTS ////////////////////////////////////////////////////
//
//		test_table_class();
////		///////////////////// Serilization tests
//
//		test_serialization();
////
////
//		///////////////// BINARY SEARCH TESTS
//    	test_binary_search();
//

    }*/
    
//    private static void test_search() throws IOException, DBAppException {
//      	String tableName = "test table";
//  		String clusteringKey = "id";
//  		int max_page_size = 128;
//  		Hashtable<String, String> colNameMin ;
//  		Hashtable<String, String> colNameMax ;
//        Hashtable<String,String> htblColNameType = new Hashtable<String,String> ( );
//        htblColNameType.put("id", "java.lang.Integer");
//        htblColNameType.put("name", "java.lang.String");
//        htblColNameType.put("gpa", "java.lang.Double");
//        Hashtable<String,String> htblColNameMin = new Hashtable<String,String> ( );
//        htblColNameMin.put("id", "0");
//        htblColNameMin.put("name", "A");
//        htblColNameMin.put("gpa", "0.0");
//        Hashtable<String,String> htblColNameMax = new Hashtable<String,String> ( );
//        htblColNameMax.put("id", "1000000");
//        htblColNameMax.put("name", "ZZZZZZZZZZZZZ");
//        htblColNameMax.put("gpa", "10.0");
//  		htblColNameType = new Hashtable();
//  		htblColNameType.put("id", "java.lang.Integer");
//  		htblColNameType.put("name", "java.lang.String");
//  		htblColNameType.put("gpa", "java.lang.double");	
//  		Table t = new Table(tableName, clusteringKey, htblColNameType, htblColNameMin, htblColNameMax,true);
//  		Hashtable htblColNameValue = new Hashtable();
//  		for (int i = 0; i < 6; i++) {
//  			htblColNameValue = new Hashtable();
//  			htblColNameValue.put("id", new Integer(i));
//  			htblColNameValue.put("name", new String("Ahmed Noor " + i));
//  			htblColNameValue.put("gpa", new Double(0.95 + i));
//  			t.insert(htblColNameValue);
//  		}
//  		
//  		
//  		
//  		
//  		System.out.println("starting table");
//  		System.out.println(t);
//  		System.out.println("searching from the most general search to the most specific");
//  		Hashtable<String, Object> search_keys = new Hashtable();
//  		
//  		System.out.println("expected results are all rows in table as the search criteria was empty so like i was looking for rows that had any value in any field");
//  		for(Hashtable<String, Object> resulting_row: t.LinearSearch(search_keys)){
//  			System.out.println(resulting_row);
//  		}
//  		
//  		htblColNameValue = new Hashtable();
//  		htblColNameValue.put("id", new Integer(8));
//  		htblColNameValue.put("name", new String("test row"));
//  		htblColNameValue.put("gpa", new Double(0.95));
//  		t.insert(htblColNameValue);
//  		
//  		search_keys.put("gpa", new Double(0.95));
//  		System.out.println("expected results are all rows with gpa 0.95 which should be 2 rows one of which is test");
//  		for(Hashtable<String, Object> resulting_row: t.LinearSearch(search_keys)){
//  			System.out.println(resulting_row);
//  		}
//  		search_keys.put("id", new Integer(0));
//
//  		System.out.println("expected results are all rows with gpa 0.95 and id = 0");
//  		for(Hashtable<String, Object> resulting_row: t.LinearSearch(search_keys)){
//  			System.out.println(resulting_row);
//  		}
//  	}
    
    static void test_table_class() throws IOException, DBAppException {
		String tableName = "test table";
		String clusteringKey = "id";
		int max_page_size = 128;
		Hashtable<String, String> colNameMin ;
		Hashtable<String, String> colNameMax ;
      Hashtable<String,String> htblColNameType = new Hashtable<String,String> ( );
      htblColNameType.put("id", "java.lang.Integer");
      htblColNameType.put("name", "java.lang.String");
      htblColNameType.put("gpa", "java.lang.Double");

      Hashtable<String,String> htblColNameMin = new Hashtable<String,String> ( );
      htblColNameMin.put("id", "0");
      htblColNameMin.put("name", "A");
      htblColNameMin.put("gpa", "0.0");

      Hashtable<String,String> htblColNameMax = new Hashtable<String,String> ( );
      htblColNameMax.put("id", "1000000");
      htblColNameMax.put("name", "ZZZZZZZZZZZ");
      htblColNameMax.put("gpa", "5.0");

		htblColNameType = new Hashtable();
		htblColNameType.put("id", "java.lang.Integer");
		htblColNameType.put("name", "java.lang.String");
		htblColNameType.put("gpa", "java.lang.double");

		Table t = new Table(tableName, clusteringKey, htblColNameType, htblColNameMax, htblColNameMax,true);
		System.out.println("<<Empty Table>>");
		System.out.println(t);
		Hashtable htblColNameValue = new Hashtable();
		htblColNameValue.put("id", new Integer(2343432));
		htblColNameValue.put("name", new String("Ahmed Noor"));
		htblColNameValue.put("gpa", new Double(0.95));

		t.insert(htblColNameValue);
		System.out.println("<<Table with entrie>>");
		System.out.println(t);

		for (int i = 0; i < max_page_size; i++) {
			htblColNameValue = new Hashtable();
			htblColNameValue.put("id", new Integer(i));
			htblColNameValue.put("name", new String("Ahmed Noor " + i));
			htblColNameValue.put("gpa", new Double(0.95 + i));

			t.insert(htblColNameValue);
		}
		System.out.println("<<Table with 1 page maxed out >>");
		System.out.println(t);

		htblColNameValue = new Hashtable();
		htblColNameValue.put("id", new Integer(max_page_size));
		htblColNameValue.put("name", new String("karim in page 2 "));
		htblColNameValue.put("gpa", new Double(1.00));

		t.insert(htblColNameValue);

		System.out.println("<<Table with 2 pages  cuz of overflow>>");
		System.out.println(t);

		htblColNameValue = new Hashtable();
		htblColNameValue.put("id", new Integer(max_page_size+1));
		htblColNameValue.put("name", new String("karim in page 2 "));
		htblColNameValue.put("gpa", new Double(1.00));

		t.insert(htblColNameValue);

		System.out.println("<<Table with 2 pages  cuz of overflow>>");
		System.out.println(t);
		
		for (int i = 0; i < 200; i++) {
			htblColNameValue = new Hashtable();
			htblColNameValue.put("id", new Integer(i));
			htblColNameValue.put("name", new String("Ahmed Noor " + i));
			htblColNameValue.put("gpa", new Double(0.95 + i));

			t.insert(htblColNameValue);
		}
		System.out.println("<<Table with few page maxed out pages and also a very agressive input algorithm>>");
		System.out.println(t);
		
		htblColNameValue = new Hashtable();
		htblColNameValue.put("id", new Integer(11));
		htblColNameValue.put("name", new String("assme"));
		htblColNameValue.put("gpa", new Double(0.9));

		t.insert(htblColNameValue);
		
		System.out.println("<<Table with few page maxed out pages and also a very agressive input algorithm>>");
		System.out.println(t);
		
    }
    
//    static void test_binary_search() {
//    	
//    	Vector<Hashtable<String, Object>> fake_pages = new Vector<Hashtable<String,Object>>();
//		 for (int i = 0; i < 10; i++) {
//			 Hashtable<String, Object> temp = new Hashtable<String, Object>();
//			 temp.put("min", i*10);
//			 temp.put("path", "page" + i);
//			 fake_pages.add(temp);
//		}
//		 System.out.println(fake_pages);
//		
//		 System.out.println("expected output is every page gets an insert from 0 to 9");
//		 for (int j = 0; j <= 10; j++) {
//		 System.out.println(binarySearch(fake_pages, 0, fake_pages.size()-1, j*10));
//		 }
//    }
    
    static void test_serialization() throws IOException {
      String strTableName = "Student";

      Hashtable<String,String> htblColNameType = new Hashtable<String,String> ( );
      htblColNameType.put("id", "java.lang.Integer");
      htblColNameType.put("name", "java.lang.String");
      htblColNameType.put("gpa", "java.lang.Double");

      Hashtable<String,String> htblColNameMin = new Hashtable<String,String> ( );
      htblColNameMin.put("id", "0");
      htblColNameMin.put("name", "A");
      htblColNameMin.put("gpa", "0.0");

      Hashtable<String,String> htblColNameMax = new Hashtable<String,String> ( );
      htblColNameMax.put("id", "1000000");
      htblColNameMax.put("name", "ZZZZZZZZZZZ");
      htblColNameMax.put("gpa", "5.0");

        Table t = new Table( strTableName, "id", htblColNameType, htblColNameMin, htblColNameMax, false);
        Hashtable record1 = new Hashtable<String, Object>();
		Hashtable record2 = new Hashtable<String, Object>();
		record1.put("id", 15);
		record2.put("id", 10);
		Page p1 = new Page(record1, t);
		String path_to_p1 = t.savePage(p1);
		Page p2 = new Page(record2, t);
		String path_to_p2 = t.savePage(p2);
		p2.insert(record1);
		t.savePage(p2);
		
		System.out.println("now print p1");
		Page p1_new = t.loadPage(path_to_p1);
		System.out.println(p1_new);

		Page p2_new = t.loadPage(path_to_p2);
		System.out.println("now print p2 with updated value to make sure it has been overwriten after save");
		System.out.println(p2_new);
    }
    
    
//	///////////Serilization part
	public String savePage(Page p) { 
		String path_to_be_saved_to = "DB/" + this.tableName;
		String path = "DB/"+this.tableName+"/"+p.ID+".ser";
		try {
			File page_file = new File(path);
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(page_file));
	        os.writeObject(p);
	        os.close(); 
	        
	    } 
	    catch (FileNotFoundException e) {
	        File dicrectory = new File("DB/" + this.tableName);
	        boolean bool = dicrectory.mkdirs();
		      if(bool){
		         System.out.println("Directory created successfully");
		      }else{
		         System.out.println("Sorry couldn’t create specified directory " + dicrectory.getAbsolutePath());
		      }
		        File f = new File(dicrectory.getAbsolutePath(), p.ID+".ser");
		        ObjectOutputStream os;
				try {
					os = new ObjectOutputStream(new FileOutputStream(f));
					 os.writeObject(p);
				     os.close(); 
				} catch (FileNotFoundException e1) {
		        	System.err.println(e1.getMessage());
					//e1.printStackTrace();
				} catch (IOException e1) {
		        	System.err.println(e1.getMessage());
					//e1.printStackTrace();
				}
		       
	    } 
	    catch (IOException e) {
	    	if(e.getMessage() != null)
        		System.err.println(e.getMessage());
	    	//e.printStackTrace();
	    }
	
		return path;
	}
	
	public Page loadPage(String path) {
		Page p = null;
		try
	      {
	         FileInputStream fileInStr = new FileInputStream(path);
	         ObjectInputStream objInStr = new ObjectInputStream(fileInStr);
	         p  = (Page) objInStr.readObject();
	         objInStr.close();
	         fileInStr.close();
	      }catch(IOException exp)
	      {
	         //exp.printStackTrace();
	        	System.err.println(exp.getMessage());
	      }catch(ClassNotFoundException cexp)
	      {
	         System.err.println("Page class not found");
	         //cexp.printStackTrace();
	      }
		return p;
		
	}
	
	
	
	
	
	
	
	
	/////////////////// UTIL
	@Override
	public String toString() {
		String str = "";
		str += "Table [tableName=" + tableName + ", clusteringKey=" + clusteringKey 
				+ "]\n";
		str += "columns in Table \n{\n";
		for (String key : colNameType.keySet()) {
			str += key + "  " + colNameType.get(key) + "\n";
		}
		str += "}\n << ==================================== >>\n";
		int dummy_index = 0;
		for (Vector<Hashtable<String, Object>> t1 : this.pages) {
			for(Hashtable<String, Object> t2: t1) {
			str += "Page " + dummy_index + " min = " + t2.get("min") + " max = " + t2.get("max");
			Page p = this.loadPage((String) t2.get("path"));
			str += "\n" + p;
			str += "------------------------------ \n";
			dummy_index++;
			}
		}

		str += "||||||||||||||||||||     |||||||||||||||||||| \n \n";
		return str;
	}

}